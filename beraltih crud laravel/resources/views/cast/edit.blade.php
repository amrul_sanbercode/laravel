@extends('adminlte.master')

@section('content')

        <!-- Default box -->
        <div class="container-fluid">
            <h2>Edit Cast {{$cast->id}}</h2>
                
        </div>
        <section class="content">
        
            <!-- Default box -->
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Cast</h3>
        
                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
                </div>
            </div>
            <div class="card-body">
                <form action="/cast/{{$cast->id}}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Masukkan Nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Umur</label>
                        <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="Masukkan Umur">
                        @error('umur')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Biodata</label>
                        <input type="text" class="form-control" value="{{$cast->bio}}" name="bio" id="bio" placeholder="Masukkan Body">
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
            <!-- /.card -->
        
        </section>
@endsection