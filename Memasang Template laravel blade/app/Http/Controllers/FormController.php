<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $nama1 = $request['FirstName'];
        $nama2 = $request['LastName'];

        return view('halaman.welcome', compact('nama1', 'nama2'));
    }
}
