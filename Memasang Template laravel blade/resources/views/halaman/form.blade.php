<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label for="FN">First name:</label><br><br>
        <input type="text" name="FirstName" id="FN"><br><br>

        <label for="LN">Last name:</label><br><br>
        <input type="text" name="LastName" id="LN"><br><br>

        <label for="">Gender:</label><br><br>
        <input type="radio" name="Gen">Male<br>
        <input type="radio" name="Gen">Female<br>
        <input type="radio" name="Gen">Other<br><br>

        <label for="Nation">Nationality:</label><br><br>
        <select name="Nationality" id="Nation">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapur">Singapur</option>
            <option value="Itali">Itali</option>
        </select><br><br>

        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name=Bahasa"" >Bahasa Indonesia <br>
        <input type="checkbox" name="English" >English <br>
        <input type="checkbox" name="Other" >Other <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" name="" id="" value="Sign Up">
    </form>
</body>
</html>